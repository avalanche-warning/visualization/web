#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script generates a simple list of .pro-file links to open in niViz.

import awio
from awset import get
import datetime
import glob
import os
import sys


def generate_from_folder(folder: str, outfilepath: str):
    hostname = get(["host", "domain"], "awsome", "/home/opera/project") # domain of awsome server
    profiles = sorted(glob.glob(folder + "/*.pro"))
    outer_folder = os.path.basename(os.path.normpath(folder))

    listing = ""
    for pro in profiles:
        profile = os.path.basename(pro)
        pro_link = f"models.{hostname}/niViz/?file=%2F{outer_folder}%2F{profile}"
        listing += f'<a href="http://{pro_link}">{profile}</a>'
        smet = pro[:-4] + ".smet"
        if os.path.exists(smet):
            smet = os.path.basename(smet)
            smet_link = f"models.{hostname}/niViz/?file=%2F{outer_folder}%2F{smet}"
            listing += f' (<a href="http://{smet_link}">{smet}</a>)'
        listing += "<br>\n"

    html_template = awio.get_scriptpath(__file__) + "/station_list.html.template"
    with open(html_template, "r") as file :
        data = file.read()
        data = data.replace("$STATION_LIST", listing[:-1])
        data = data.replace("$TIMESTAMP", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        with open(outfilepath, "w") as outfile:
            outfile.write(data)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        sys.exit("[E] Synopsis: python3 generate_station_list.py <pro_folder> <output_folder>")
    generate_from_folder(sys.argv[1], sys.argv[2])
